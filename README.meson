# Building with meson:

Get meson 0.47, either from your distro's package manager, or using pip:

```
python3 -m pip install meson --user
```

Get ninja from a package manager (dnf, pacman, brew, ..) on Linux
and MacOS, or with the installer on Windows, then in FFmpeg:

```
meson build && ninja -C build
```

Build FFmpeg without external dependencies:
```
meson --auto-features=disabled build && ninja -C build
```

Known to build on Debian, Arch Linux and MacOS High Sierra.

# Running the tests:

```
meson test -C build
```

# Updating the FFmpeg base snapshot of the Meson port

It's complicated and at the moment not fully documented.

It should only be needed when updating from one feature version to another,
not for minor bug fix releases (assuming there aren't too many changes in
`configure` between minor bug fix releases).

The procedure is generally to run four scripts (
`parse_sources.py`, `find_things_extern.py`, `find_things.py` and
`recursive_selects.py`), replace the current generated files with them, but
make sure to bring back in the manual changes (that's the tedious part).

- `recursive_selects.py` works in place, but you need to check the diff and
  revert some of the changes.

- `parse_sources.py` updates the meson.build files trying to keep manually written parts
  as specified by the `#### --- [END] GENERATED --- ####` specifiers

- `find_things_extern.py` and `find_things.py` update the meson_options.txt file trying to keep manually written parts
  as specified by the `#### --- [END] GENERATED [EXTERN|FILTER] OPTIONS --- ####` specifiers, respectively
  - run the scripts with the flag `--update-options` to trigger the update

Then bump versions and check what changed in `configure` since the last time,
which is the other complicated bit, as it requires understanding of the
somewhat complex ffmpeg build system and of the way it was reimplemented
for the Meson port. Some documentation of that is at the top of `depgraph.py`.

Good luck!
